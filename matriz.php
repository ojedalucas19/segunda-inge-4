<?php

require 'vendor/autoload.php';

use Symfony\Component\VarDumper\VarDumper;

function generarMatrizAleatoria() {
    $filas = rand(2, 5);
    $columnas = rand(2, 5);
    $matriz = [];

    for ($i = 0; $i < $filas; $i++) {
        for ($j = 0; $j < $columnas; $j++) {
            $matriz[$i][$j] = rand(1, 10) * 2 - 1; // Números impares
        }
    }

    return $matriz;
}

function imprimirMatriz($matriz) {
    foreach ($matriz as $fila) {
        echo "[ ";
        foreach ($fila as $valor) {
            printf("%2d ", $valor);
        }
        echo "]" . PHP_EOL;
    }
}

$matrizAleatoria = generarMatrizAleatoria();
imprimirMatriz($matrizAleatoria);
